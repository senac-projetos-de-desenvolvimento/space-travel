'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('/signup', 'UserController.signup')

Route.post('/signin', 'UserController.signin')

Route.get('/user/:id', 'UserController.show')
  .middleware('auth')

Route.post('/user/:id/infos', 'UserController.infosAdd')
  .middleware('auth')

Route.resource('trips', 'TripController')
  .apiOnly()
  .middleware('auth')

Route.post('/trips/:id/backpack', 'BackpackController.store')
  .middleware('auth')
