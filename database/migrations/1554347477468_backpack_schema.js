'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BackpackSchema extends Schema {
  up () {
    this.create('backpacks', (table) => {
      table.increments()
      table.string('nickname', 20).notNullable()
      table.string('size').notNullable()
      table.integer('cap')
      table.integer('tshirt')
      table.integer('sweatshirt')
      table.integer('long_shirt')
      table.integer('jacket')
      table.integer('socks')
      table.integer('underwear')
      table.integer('shorts')
      table.integer('pants')
      table.integer('shoes')
      table.integer('boots')
      table.integer('slipper')
      table
        .integer('trip_id')
        .unsigned()
        .references('id')
        .inTable('trips')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
        table.timestamps()
    })
  }

  down () {
    this.drop('backpacks')
  }
}

module.exports = BackpackSchema
