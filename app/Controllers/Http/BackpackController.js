'use strict'

const Trip = use("App/Models/Trip")
const Backpack = use("App/Models/Backpack")

/*
 * Resourceful controller for interacting with backpacks
 */
class BackpackController {
  async store ({ params, auth ,request }) {
    const trip = await Trip.findOrFail(params.id)

    const { id } = auth.user

    const data = request.only([
      'nickname',
      'size',
      'cap',
      'tshirt',
      'sweatshirt',
      'long_shirt',
      'jacket',
      'socks',
      'underwear',
      'shorts',
      'pants',
      'shoes',
      'boots',
      'slipper'
    ])

    const backpack = await Backpack.create({ ...data, user_id: id, trip_id: trip.id})

    return backpack

  }

  async show ({ params }) {
    const backpack = await Backpack.findOrFail(params.id)

    return backpack

  }

  async update ({ params, request }) {
    const backpack = await Backpack.findOrFail(params.id)

    const data = request.only([
      'nickname',
      'size',
      'cap',
      'tshirt',
      'sweatshirt',
      'long_shirt',
      'jacket',
      'socks',
      'underwear',
      'shorts',
      'pants',
      'shoes',
      'boots',
      'slipper'
    ])

    backpack.merge(data)

    await backpack.save()

    return backpack
  }

  async destroy ({ params, auth, response }) {
    const backpack = await Backpack.findOrFail(params.id)

    if (backpack.user_id !== auth.user.id) {
      return response.status(401).send({ error: 'Você não possui permissão!'})
    }

    await backpack.delete()
  }
}

module.exports = BackpackController
