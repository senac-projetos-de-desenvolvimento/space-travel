'use strict'

const User = use("App/Models/User")
const PersonalInfo = use("App/Models/PersonalInfo")

class UserController {
    async signup ({ request }) {
        const data = request.only([ "username", "email", "password" ])

        const user = await User.create(data)

        return user
    }

    async signin ({ request, auth }) {
      const { email, password } = request.all()

      const token = await auth.attempt(email, password)
      
      const user = await User.findBy('email', email)
    
      return { token, user }
  }

  async show ({ params }) {
    const user = await User.findOrFail(params.id)

    await user.load('personalInfos')
    await user.load('trips')
    await user.load('backpacks')

    return user

  }

  async infosAdd ({ params, request }) {
    const user = await User.findOrFail(params.id)

    const data = request.only([
      'age',
      'phone',
      'address',
      'cep',
      'food_1',
      'food_2',
      'food_3',
      'music_1',
      'music_2',
      'music_3',
      'local_1',
      'local_2',
      'local_3',
    ])

    const personalInfos = await PersonalInfo.create({ ...data, user_id: user.id})

    return personalInfos
  }

  async update ({ params, request }) {
    const user  = await User.findOrFail(params.id)

    const data = request.only([
      "username",
      "email",
      "password"
    ])

    user.merge(data)

    await user.save()

    return user
  }

  async infosUpdate ({ params, request, response, auth }) {
    if (auth.user.id !== Number(params.user_id)) {
      return response.status(401).send({ error: 'Você não possui permissão!'})
    }

    const infos = await PersonalInfo.findOrFail(params.id)

    const data = request.only([
      'age',
      'phone',
      'address',
      'cep',
      'food_1',
      'food_2',
      'food_3',
      'music_1',
      'music_2',
      'music_3',
      'local_1',
      'local_2',
      'local_3',
    ])

    infos.merge(data)

    await infos.save()

    return infos
  }

  async destroy ({ params, auth, response }) {
    const User = await User.findOrFail(params.id)

    if (user.id !== auth.user.id) {
      return response.status(401).send({ error: 'Você não possui permissão!'})
    }

    await user.delete()
  }

}

module.exports = UserController
