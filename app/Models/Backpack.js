'use strict'

const Model = use('Model')

class Backpack extends Model {
  user () {
    return hasOne('App/Models/User')
  }
}

module.exports = Backpack
